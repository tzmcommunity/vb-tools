#!/bin/bash

echo WATCH
date


mkdir -p cd ~/template
cd ~/template

if OUTPUT=$(rsync -a --info=NAME -ur /git/chapter.site.template/* .)
then
    if [ "$OUTPUT" != "" ]                   # got output?
    then
		echo OUTPUT $OUTPUT

		ps aux | grep jekyll
		killall bundle
		
		if [  ! -d .vendor/bundle ] || [ "Gemfile" -nt "Gemfile.lock" ]; then
			bundle install --path .vendor/bundle
		fi

		GEM_PATH=${GEM_PATH}:.vendor/bundle/ruby/2.5.0 bundle exec jekyll build -d /opt/sites/test 2> error.log
		
		buildexit=$?
		
		echo exit code "${buildexit}"
		if [ "0" == "${buildexit}" ]; then
			echo OK?
			if [ -f error.log ]; then rm error.log; fi
			sleep 1
		else 
			echo FAIL?
			sleep 1
		fi
	else
		if [ -f error.log ]; then cat error.log; fi
    fi
fi
