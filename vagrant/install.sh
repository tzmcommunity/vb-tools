#!/bin/bash

sudo apt-get -y install software-properties-common

curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get update

sudo apt-get install -y npm
sudo apt-get install -y nodejs
sudo apt-get install -y nginx
sudo apt-get install -y jekyll
sudo apt-get install -y python-pip
sudo apt-get install -y apache2

pip install --user configparser

sudo useradd site_builder -m 
echo site_builder:password | sudo chpasswd

sudo apt-get install -y ruby-dev

sudo gem install bundler

sudo pip install configparser
sudo pip install GitPython

sudo killall nginx
sudo apt-get -y remove nginx

sudo mkdir /opt/sites
sudo chown site_builder.www-data /opt/sites -R

sudo rm /etc/apache2/site-enabled/000-default.conf
sudo cp /vagrant/apache/* /etc/apache2/ -ruv
sudo /etc/init.d/apache2 restart


